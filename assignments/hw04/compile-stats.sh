#!/bin/sh
#compile-stats.sh

isimp loop-test-O0
awk 'BEGIN {RS="\n"; FS=","} {if(NR == 1){X=$2; print $1","$2} else{print $1","$2/X}}' stats.csv > stats0.txt
isimp loop-test-O1
awk 'BEGIN {RS="\n"; FS=","} {if(NR == 1){X=$2; print $1","$2} else{print $1","$2/X}}' stats.csv > stats1.txt
isimp loop-test-O2
awk 'BEGIN {RS="\n"; FS=","} {if(NR == 1){X=$2; print $1","$2} else{print $1","$2/X}}' stats.csv > stats2.txt
isimp loop-test-O3
awk 'BEGIN {RS="\n"; FS=","} {if(NR == 1){X=$2; print $1","$2} else{print $1","$2/X}}' stats.csv > stats3.txt

isimp bubble-rand
awk 'BEGIN {RS="\n"; FS=","} {if(NR == 1){X=$2; print $1","$2} else{print $1","$2/X}}' stats.csv > bubblestats.txt
isimp btree-rand
awk 'BEGIN {RS="\n"; FS=","} {if(NR == 1){X=$2; print $1","$2} else{print $1","$2/X}}' stats.csv > btreestats3.txt



